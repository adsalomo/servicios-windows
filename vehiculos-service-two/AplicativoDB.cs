namespace Service_Maintenace
{
    using System.Data.Entity;

    public partial class AplicativoDB : DbContext
    {
        public AplicativoDB()
            : base("name=AplicativoDB")
        {
        }

        public virtual DbSet<Parametro> parametros { get; set; }
        public virtual DbSet<Maintenace> maintenace { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
