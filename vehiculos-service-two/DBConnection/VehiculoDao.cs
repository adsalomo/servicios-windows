﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Service_Maintenace.DBConnection
{
    public class MaintenaceDao : IMaintenaceDao
    {
        public void Guardar(Maintenace maintenace)
        {
            string cadenaCon = System.Configuration.ConfigurationManager.ConnectionStrings["AplicativoDB"].ConnectionString;
            using (SqlConnection sqlConnection = new SqlConnection(cadenaCon))
            {
                sqlConnection.Open();

                string sql = "INSERT INTO mantenimiento (id, code, typeName, brandName, lineName, city_id, city_name, city_code, costCenter_id, " +
                             "costCenter_name, costCenter_code, primaryGroup, secundaryGroup, qtyFailure, odometerTravaled, hoursInMaintenance, mtbf, mttrHrs, reliability, start_date) " +
                             "VALUES (" +
                             "@param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10, @param11, @param12, " +
                             "@param13, @param14, @param15, @param16, @param17, @param18, @param19, @param20)";
                using (SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection))
                {
                    sqlCommand.Parameters.Add("@param1", SqlDbType.BigInt).Value = maintenace.id;
                    sqlCommand.Parameters.Add("@param2", SqlDbType.NVarChar).Value = maintenace.code ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param3", SqlDbType.NVarChar).Value = maintenace.typeName ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param4", SqlDbType.NVarChar).Value = maintenace.brandName ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param5", SqlDbType.NVarChar).Value = maintenace.lineName ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param6", SqlDbType.NVarChar).Value = maintenace.cityId ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param7", SqlDbType.NVarChar).Value = maintenace.cityName ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param8", SqlDbType.NVarChar).Value = maintenace.cityCode ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param9", SqlDbType.NVarChar).Value = maintenace.costCenterId ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param10", SqlDbType.NVarChar).Value = maintenace.costCenterName ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param11", SqlDbType.NVarChar).Value = maintenace.costCenterCode ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param12", SqlDbType.NVarChar).Value = maintenace.primaryGroup ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param13", SqlDbType.NVarChar).Value = maintenace.secundaryGroup ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param14", SqlDbType.NVarChar).Value = maintenace.qtyFailure ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param15", SqlDbType.NVarChar).Value = maintenace.odometerTravaled ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param16", SqlDbType.NVarChar).Value = maintenace.hoursInMaintenance ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param17", SqlDbType.NVarChar).Value = maintenace.mtbf ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param18", SqlDbType.NVarChar).Value = maintenace.mttrHrs ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param19", SqlDbType.NVarChar).Value = maintenace.reliability ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param20", SqlDbType.DateTime).Value = maintenace.FechaProceso;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }
    }
}
