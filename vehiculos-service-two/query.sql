INSERT INTO parametros(Parametro, Valor) values ('CLOUDFLEET.MAINTENACE.FECHA_DESDE', 'getdate-1');
INSERT INTO parametros(Parametro, Valor) values ('CLOUDFLEET.MAINTENACE.FECHA_HASTA', 'getdate');
INSERT INTO parametros(Parametro, Valor) values ('CLOUDFLEET.MAINTENACE.SLEEP', '5000'); -- 5 segundos
INSERT INTO parametros(Parametro, Valor) values ('CLOUDFLEET.MAINTENACE.TIMER', '8640000'); -- 5 segundos

INSERT INTO parametros(Parametro, Valor) values ('CLOUDFLEET.MAINTENACE.INITIAL.MONTH', '1'); -- 5 segundos
INSERT INTO parametros(Parametro, Valor) values ('CLOUDFLEET.MAINTENACE.FINAL.MONTH', '2'); -- 5 segundos
INSERT INTO parametros(Parametro, Valor) values ('CLOUDFLEET.MAINTENACE.YEAR', '2019'); -- 5 segundos

USE [Aplicativos]
GO

/****** Object:  Table [dbo].[maintenace]    Script Date: 29/11/2019 11:19:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[mantenimiento](
	[id] [bigint] NOT NULL,
	[code] [nvarchar](max) NULL,
	[typeName] [nvarchar](max) NULL,
	[brandName] [nvarchar](max) NULL,
	[lineName] [nvarchar](max) NULL,
	[city_id] [nvarchar](max) NULL,
	[city_name] [nvarchar](max) NULL,
	[city_code] [nvarchar](max) NULL,
	[costCenter_id] [nvarchar](max) NULL,
	[costCenter_name] [nvarchar](max) NULL,
	[costCenter_code] [nvarchar](max) NULL,
	[primaryGroup] [nvarchar](max) NULL,
	[secundaryGroup] [nvarchar](max) NULL,
	[qtyFailure] [nvarchar](max) NULL,
	[odometerTravaled] [nvarchar](max) NULL,
	[hoursInMaintenance] [nvarchar](max) NULL,
	[mtbf] [nvarchar](max) NULL,
	[mttrHrs] [nvarchar](max) NULL,
	[reliability] [nvarchar](max) NULL,
	[start_date] [datetime] NULL                                          
)


