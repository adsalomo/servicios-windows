namespace Service_Maintenace
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
 
    [Table("mantenimiento")]
    public partial class Maintenace
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        public string code { get; set; }
        
        public string typeName { get; set; }
        
        public string brandName { get; set; }
        
        public string lineName { get; set; }

        [Column("city_id")]
        public string cityId { get; set; }

        [Column("city_name")]
        public string cityName { get; set; }

        [Column("city_code")]
        public string cityCode { get; set; }

        [Column("costCenter_id")]
        public string costCenterId { get; set; }

        [Column("costCenter_name")]
        public string costCenterName { get; set; }

        [Column("costCenter_code")]
        public string costCenterCode { get; set; }

        public string primaryGroup { get; set; }

        public string secundaryGroup { get; set; }

        public string qtyFailure { get; set; }

        public string odometerTravaled { get; set; }

        public string hoursInMaintenance { get; set; }

        public string mtbf { get; set; }

        public string mttrHrs { get; set; }

        public string reliability { get; set; }

        [Column("start_date")]
        public DateTime FechaProceso { get; set; }
    }
}
