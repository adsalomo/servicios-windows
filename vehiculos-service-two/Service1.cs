﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using RestSharp;
using Newtonsoft.Json;
using Service_Maintenace.DBConnection;

namespace Service_Maintenace
{
    public partial class Service1 : ServiceBase
    {
        private Timer timer = new Timer();
        private AplicativoDB db = new AplicativoDB();
        private readonly string KEY_NEXT_PAGE = "X-NextPage";
        private string fecha_desde = "";
        private string fecha_hasta = "";
        private IMaintenaceDao vehiculoDao = new MaintenaceDao();

        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            WriteToFile("Service is recall at " + DateTime.Now);

            int initialMonth = int.Parse(ObtenerParametro("CLOUDFLEET.MAINTENACE.INITIAL.MONTH"));
            int finalMonth = int.Parse(ObtenerParametro("CLOUDFLEET.MAINTENACE.FINAL.MONTH"));
            int year = int.Parse(ObtenerParametro("CLOUDFLEET.MAINTENACE.YEAR"));
            string token = "Bearer " + ObtenerParametro("CLOUDFLEET.TOKEN");
            int timeSleep = int.Parse(ObtenerParametro("CLOUDFLEET.MAINTENACE.SLEEP"));

            for (int i = initialMonth; i <= finalMonth; i++)
            {
                int days = DateTime.DaysInMonth(year, i);

                string fromDate = year + "-" + i.ToString().PadLeft(2, '0') + "-01T00:00:00-05:00";
                Console.WriteLine(fromDate);

                string toDate = year + "-" + i.ToString().PadLeft(2, '0') + "-" + days.ToString().PadLeft(2, '0') + "T23:59:59-05:00";
                Console.WriteLine(toDate);

                string processDate = year + "-" + i.ToString().PadLeft(2, '0') + "-" + days.ToString().PadLeft(2, '0') + " 23:59:59";
                Console.WriteLine(processDate);

                DateTime processDateConvert = DateTime.Parse(processDate);
                Console.WriteLine(processDateConvert);

                WriteToFile("Inicio día " + fromDate);

                string url = "https://fleet.cloudfleet.com/api/v1/maintenace-time?dateFrom=" + fromDate + "&dateTo=" + toDate + "&vehicleMeasureUnit=hours";
                EjecutarProceso(url, token, processDateConvert, timeSleep);

                WriteToFile("Fin día " + fromDate);

            }

            WriteToFile("Finalizo la busqueda y actualizacion " + DateTime.Now);

            /*WriteToFile("Service is recall at " + DateTime.Now);
            Console.WriteLine("Service is recall at " + DateTime.Now);
            fecha_desde = ConvertirFecha(ObtenerParametro("CLOUDFLEET.MAINTENACE.FECHA_DESDE"), "From");
            fecha_hasta = ConvertirFecha(ObtenerParametro("CLOUDFLEET.MAINTENACE.FECHA_HASTA"), "To");

            DateTime fechaProceso = DateTime.Parse(ConvertirFecha(ObtenerParametro("CLOUDFLEET.MAINTENACE.FECHA_HASTA"), ""));

            int timeSleep = int.Parse( ObtenerParametro("CLOUDFLEET.MAINTENACE.SLEEP"));

            WriteToFile("fecha_desde: " + fecha_desde);
            WriteToFile("fecha_hasta: " + fecha_hasta);
            string token = "Bearer " + ObtenerParametro("CLOUDFLEET.TOKEN");
            string url = "https://fleet.cloudfleet.com/api/v1/maintenace-time?dateFrom="
                + fecha_desde + "&dateTo=" + fecha_hasta + "&vehicleMeasureUnit=hours";
            EjecutarProceso(url, token, fechaProceso, timeSleep);
            WriteToFile("Finalizo la busqueda y actualizacion " + DateTime.Now);
            Console.WriteLine("Finalizo la busqueda y actualizacion " + DateTime.Now);*/
        }

        private void EjecutarProceso(string url, string token, DateTime fechaProceso, int timeSleep)
        {
            try
            {
                WriteToFile("Ejecutando url: " + url);
                Console.WriteLine("Ejecutando url: " + url);

                IRestResponse response = EjecutarGet(url, token);
                if (ValidarRespuesta(response))
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(response.Content);

                    foreach (var item in dynJson)
                    {
                        WriteToFile("Objeto a validar: " + item);
                        Maintenace maintenaceJSON = new Maintenace();
                        maintenaceJSON.id = item.id;
                        maintenaceJSON.code = item.code;
                        maintenaceJSON.typeName = item.typeName;
                        maintenaceJSON.brandName = item.brandName;
                        maintenaceJSON.lineName = item.lineName;

                        if (item.city != null)
                        {
                            maintenaceJSON.cityId = item.city.id;
                            maintenaceJSON.cityName = item.city.name;
                            maintenaceJSON.cityCode = item.city.code;
                        }

                        if (item.costCenter != null)
                        {
                            maintenaceJSON.costCenterId = item.costCenter.id;
                            maintenaceJSON.costCenterName = item.costCenter.name;
                            maintenaceJSON.costCenterCode = item.costCenter.code;
                        }

                        maintenaceJSON.primaryGroup = item.primaryGroup;
                        maintenaceJSON.secundaryGroup = item.secundaryGroup;
                        maintenaceJSON.qtyFailure = item.qtyFailure;
                        maintenaceJSON.odometerTravaled = item.odometerTravaled;
                        maintenaceJSON.hoursInMaintenance = item.hoursInMaintenance;
                        maintenaceJSON.mtbf = item.mtbf;
                        maintenaceJSON.mttrHrs = item.mttrHrs;
                        maintenaceJSON.reliability = item.reliability;
                        maintenaceJSON.FechaProceso = fechaProceso;

                        vehiculoDao.Guardar(maintenaceJSON);
                    }

                    if (response.Headers != null && response.Headers.ToList().Count > 0)
                    {
                        var headers = response.Headers.ToList().Find(x => x.Name.Equals(KEY_NEXT_PAGE));
                        if (headers != null && headers.Value != null)
                        {
                            System.Threading.Thread.Sleep(timeSleep);
                            EjecutarProceso(Convert.ToString(headers.Value), token, fechaProceso, timeSleep);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.InnerException != null && ex.InnerException.InnerException.Message != null)
                {
                    WriteToFile("Error InnerException " + ex.InnerException.InnerException.Message);
                    Console.WriteLine(ex.InnerException.Message);
                }
                Console.WriteLine("Error general " + ex.StackTrace.ToString());
                WriteToFile("Error general " + ex.StackTrace.ToString());
            }
        }

        #region "Utilidades"
        private static IRestResponse EjecutarGet(string url, string security)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");

            request.AddHeader("accept-encoding", "gzip, deflate");
            request.AddHeader("Host", "fleet.cloudfleet.com");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
            request.AddHeader("Authorization", security);
            request.AddHeader("Content-Type", "application/json; charset=utf-8");
            IRestResponse response = client.Execute(request);
            return response;
        }

        private bool ValidarRespuesta(IRestResponse response)
        {
            WriteToFile("Se procede a validar el objeto obtenido del servicio.");
            if (response != null)
            {
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    dynamic dynJsonerror = JsonConvert.DeserializeObject(response.Content);
                    if (dynJsonerror != null)
                    {
                        foreach (var item in dynJsonerror)
                        {
                            if (item != null)
                            {
                                string mensaje = ((Newtonsoft.Json.Linq.JContainer)item.First).First.ToString();
                                WriteToFile("La Api devolvio un mensaje de error: " + mensaje);
                            }
                        }
                    }

                    if (response.ErrorException != null && !response.ErrorException.Equals(""))
                    {
                        WriteToFile("Error al consumir servicio: " + response.ErrorException);
                    }

                    if (response.ErrorMessage != null && !response.ErrorMessage.Equals(""))
                    {
                        WriteToFile("Error al consumir servicio: " + response.ErrorMessage);
                    }

                    return false;
                }
            }

            WriteToFile("Objeto validado sin errores.");
            return true;
        }

        private string ObtenerParametro(string pparametro)
        {
            AplicativoDB dbparametro = new AplicativoDB();
            string valor = dbparametro.parametros.Where(x => x.Parametro1 == pparametro).First().Valor;
            dbparametro.Dispose();
            return valor;
        }

        private string ConvertirFecha(string pvalor, string type)
        {
            DateTime fecha;
            if (pvalor.ToUpper() == "GETDATE")
            {
                fecha = DateTime.Today;
            }
            else
            {
                string dias = pvalor.ToUpper().Replace("GETDATE", "");
                fecha = DateTime.Today.AddDays(Convert.ToInt16(dias));
            }

            string fechaString = fecha.Year.ToString() + '-' + fecha.Month.ToString().PadLeft(2, '0') + '-' + fecha.Day.ToString().PadLeft(2, '0');

            if (type.Equals("From"))
            {
                fechaString += "T00:00:00-05:00";
            }
            else if (type.Equals("To"))
            {
                fechaString += "T23:59:59-05:00";
            }
            else
            {
                fechaString += " 23:59:59";
            }

            return fechaString;
        }

        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceVehiculoLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
        #endregion

        #region "Timer"
        protected override void OnStart(string[] args)
        {
            WriteToFile("Service is started at " + DateTime.Now);
            WriteToFile("Version 1.0.4");
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = Convert.ToDouble(ObtenerParametro("CLOUDFLEET.MAINTENACE.TIMER"));
            WriteToFile("intervalo: " + timer.Interval);
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            OnDebug();
        }
        #endregion
    }
}
