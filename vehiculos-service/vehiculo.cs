namespace Service_Maintenace
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vehiculos")]
    public partial class Vehiculo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        //[StringLength(20)]
        public string code { get; set; }

        //[StringLength(100)]
        public string typeName { get; set; }

        //[StringLength(40)]
        public string brandName { get; set; }

        //[StringLength(40)]
        public string lineName { get; set; }

        [Column("city.id")]
        //[StringLength(20)]
        public string city_id { get; set; }

        [Column("city.name")]
        //[StringLength(20)]
        public string city_name { get; set; }

        [Column("city.code")]
        //[StringLength(20)]
        public string city_code { get; set; }

        [Column("costCenter.id")]
        //[StringLength(20)]
        public string costCenter_id { get; set; }

        [Column("costCenter.name")]
        //[StringLength(20)]
        public string costCenter_name { get; set; }

        [Column("costCenter.code")]
        //[StringLength(20)]
        public string costCenter_code { get; set; }

        //[StringLength(100)]
        public string primaryGroup { get; set; }

        //[StringLength(100)]
        public string secundaryGroup { get; set; }

        //[StringLength(20)]
        public string hoursShouldWork { get; set; }

        //[StringLength(20)]
        public string hoursInMaintenance { get; set; }

        //[StringLength(50)]
        public string availability { get; set; } 

        [Column("start_date")]
        public DateTime FechaProceso { get; set; }
    }
}
