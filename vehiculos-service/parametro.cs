namespace Service_Maintenace
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("parametros")]
    public partial class Parametro
    {
        [Key]
        [Column("Parametro")]
        [StringLength(200)]
        public string Parametro1 { get; set; }

        [Required]
        [StringLength(200)]
        public string Valor { get; set; }
    }
}
