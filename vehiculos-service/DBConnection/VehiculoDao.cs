﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Service_Maintenace.DBConnection
{
    public class VehiculoDao : IVehiculoDao
    {
        public void Guardar(Vehiculo vehiculo)
        {
            string cadenaCon = System.Configuration.ConfigurationManager.ConnectionStrings["AplicativoDB"].ConnectionString;
            using (SqlConnection sqlConnection = new SqlConnection(cadenaCon))
            {
                sqlConnection.Open();
                string sql = "INSERT INTO vehiculos (id, code, typeName, brandName, lineName, [city.id], [city.name], [city.code], [costCenter.id], " +
                             "[costCenter.name], [costCenter.code], primaryGroup, secundaryGroup, hoursShouldWork, hoursInMaintenance, " +
                             "availability, start_date) VALUES (" +
                             "@param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10, @param11, @param12, " +
                             "@param13, @param14, @param15, @param16, @param17)";
                using (SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection))
                {
                    sqlCommand.Parameters.Add("@param1", SqlDbType.BigInt).Value = vehiculo.id;
                    sqlCommand.Parameters.Add("@param2", SqlDbType.NVarChar).Value = vehiculo.code ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param3", SqlDbType.NVarChar).Value = vehiculo.typeName ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param4", SqlDbType.NVarChar).Value = vehiculo.brandName ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param5", SqlDbType.NVarChar).Value = vehiculo.lineName ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param6", SqlDbType.NVarChar).Value = vehiculo.city_id ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param7", SqlDbType.NVarChar).Value = vehiculo.city_name ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param8", SqlDbType.NVarChar).Value = vehiculo.city_code ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param9", SqlDbType.NVarChar).Value = vehiculo.costCenter_id ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param10", SqlDbType.NVarChar).Value = vehiculo.costCenter_name ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param11", SqlDbType.NVarChar).Value = vehiculo.costCenter_code ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param12", SqlDbType.NVarChar).Value = vehiculo.primaryGroup ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param13", SqlDbType.NVarChar).Value = vehiculo.secundaryGroup ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param14", SqlDbType.NVarChar).Value = vehiculo.hoursShouldWork ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param15", SqlDbType.NVarChar).Value = vehiculo.hoursInMaintenance ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param16", SqlDbType.NVarChar).Value = vehiculo.availability ?? (object)DBNull.Value;
                    sqlCommand.Parameters.Add("@param17", SqlDbType.DateTime).Value = vehiculo.FechaProceso;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }
    }
}
