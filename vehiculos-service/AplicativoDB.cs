namespace Service_Maintenace
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AplicativoDB : DbContext
    {
        public AplicativoDB()
            : base("name=AplicativoDB")
        {
        }

        public virtual DbSet<Parametro> parametros { get; set; }
        public virtual DbSet<Vehiculo> vehiculos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
