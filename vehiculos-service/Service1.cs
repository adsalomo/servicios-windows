﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using RestSharp;
using Newtonsoft.Json;
using Service_Maintenace.DBConnection;

namespace Service_Maintenace
{
    public partial class Service1 : ServiceBase
    {
        private Timer timer = new Timer();  
        private AplicativoDB db = new AplicativoDB();
        private readonly string KEY_NEXT_PAGE = "X-NextPage";
        private string fecha_desde = "";
        private string fecha_hasta = "";
        private IVehiculoDao vehiculoDao = new VehiculoDao();

        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            WriteToFile("Service is recall at " + DateTime.Now);
            Console.WriteLine("Service is recall at " + DateTime.Now);
            fecha_desde = ConvertirFecha(ObtenerParametro("CLOUDFLEET.VEHICULO.FECHA_DESDE"));
            fecha_hasta = ConvertirFecha(ObtenerParametro("CLOUDFLEET.VEHICULO.FECHA_HASTA"));
            WriteToFile("fecha_desde: " + fecha_desde);
            WriteToFile("fecha_hasta: " + fecha_hasta);
            string token = "Bearer " + ObtenerParametro("CLOUDFLEET.TOKEN");
            string url = "https://fleet.cloudfleet.com/api/v1/availability?dateFrom=" + fecha_desde + "&dateTo=" + fecha_hasta;
            EjecutarProceso(url, token, DateTime.Now.AddDays(-1));
            WriteToFile("Finalizo la busqueda y actualizacion " + DateTime.Now);
            Console.WriteLine("Finalizo la busqueda y actualizacion " + DateTime.Now);
        }

        private void EjecutarProceso(string url, string token, DateTime fechaProceso)
        {
            try
            {
                WriteToFile("Ejecutando url: " + url);
                Console.WriteLine("Ejecutando url: " + url);

                IRestResponse response = EjecutarGet(url, token);
                if (ValidarRespuesta(response))
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(response.Content);

                    foreach (var item in dynJson)
                    {
                        //WriteToFile("Objeto a validar: " + item);
                        Vehiculo vehiculojson = new Vehiculo();
                        vehiculojson.id = item.id;
                        vehiculojson.code = item.code;
                        vehiculojson.typeName = item.typeName;
                        vehiculojson.brandName = item.brandName;
                        vehiculojson.lineName = item.lineName;

                        if (item.city != null)
                        {
                            vehiculojson.city_id = item.city.id;
                            vehiculojson.city_name = item.city.name;
                            vehiculojson.city_code = item.city.code;
                        }

                        if (item.costCenter != null)
                        {
                            vehiculojson.costCenter_id = item.costCenter.id;
                            vehiculojson.costCenter_name = item.costCenter.name;
                            vehiculojson.costCenter_code = item.costCenter.code;
                        }

                        vehiculojson.primaryGroup = item.primaryGroup;
                        vehiculojson.secundaryGroup = item.secundaryGroup;
                        vehiculojson.hoursShouldWork = item.hoursShouldWork;
                        vehiculojson.hoursInMaintenance = item.hoursInMaintenance;
                        vehiculojson.availability = item.availability;
                        vehiculojson.FechaProceso = fechaProceso;

                        // Persiste en bd
                        vehiculoDao.Guardar(vehiculojson);
                        /*db.vehiculos.Attach(vehiculojson);
                        db.Entry(vehiculojson).State = EntityState.Modified;
                        db.SaveChanges();*/
                    }

                    if (response.Headers != null && response.Headers.ToList().Count > 0)
                    {
                        var headers = response.Headers.ToList().Find(x => x.Name.Equals(KEY_NEXT_PAGE));
                        if (headers != null && headers.Value != null)
                        {
                            EjecutarProceso(Convert.ToString(headers.Value), token, fechaProceso);
                        }
                    }
                }
            }
            //catch (DbEntityValidationException ex)
            //{
            //    foreach (var eve in ex.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        WriteToFile("Error de Persistencia:" + string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ", eve.Entry.Entity.GetType().Name, eve.Entry.State));
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
            //            WriteToFile(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
            //        }
            //    }
            //}
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.InnerException != null && ex.InnerException.InnerException.Message != null)
                {
                    WriteToFile("Error InnerException " + ex.InnerException.InnerException.Message);
                    Console.WriteLine(ex.InnerException.Message);
                }
                Console.WriteLine("Error general " + ex.StackTrace.ToString());
                WriteToFile("Error general " + ex.StackTrace.ToString());
            }
        }

        #region "Utilidades"
        private static IRestResponse EjecutarGet(string url, string security)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");

            request.AddHeader("accept-encoding", "gzip, deflate");
            request.AddHeader("Host", "fleet.cloudfleet.com");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
            request.AddHeader("Authorization", security);
            request.AddHeader("Content-Type", "application/json; charset=utf-8");
            IRestResponse response = client.Execute(request);
            return response;
        }

        private bool ValidarRespuesta(IRestResponse response)
        {
            WriteToFile("Se procede a validar el objeto obtenido del servicio.");
            if (response != null)
            {
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    dynamic dynJsonerror = JsonConvert.DeserializeObject(response.Content);
                    if (dynJsonerror != null)
                    {
                        foreach (var item in dynJsonerror)
                        {
                            if (item != null)
                            {
                                string mensaje = ((Newtonsoft.Json.Linq.JContainer)item.First).First.ToString();
                                WriteToFile("La Api devolvio un mensaje de error: " + mensaje);
                            }
                        }
                    }

                    if (response.ErrorException != null && !response.ErrorException.Equals(""))
                    {
                        WriteToFile("Error al consumir servicio: " + response.ErrorException);
                    }

                    if (response.ErrorMessage != null && !response.ErrorMessage.Equals(""))
                    {
                        WriteToFile("Error al consumir servicio: " + response.ErrorMessage);
                    }

                    return false;
                }
            }

            WriteToFile("Objeto validado sin errores.");
            return true;
        }

        private string ObtenerParametro(string pparametro)
        {
            AplicativoDB dbparametro = new AplicativoDB();
            string valor = dbparametro.parametros.Where(x => x.Parametro1 == pparametro).First().Valor;
            dbparametro.Dispose();
            return valor;
        }

        private string ConvertirFecha(string pvalor)
        {
            DateTime fecha;
            if (pvalor.ToUpper() == "GETDATE")
            {
                fecha = DateTime.Today;
            }
            else
            {
                string dias = pvalor.ToUpper().Replace("GETDATE", "");
                fecha = DateTime.Today.AddDays(Convert.ToInt16(dias));
            }
            return fecha.Year.ToString() + '-' + fecha.Month.ToString().PadLeft(2, '0') + '-' + fecha.Day.ToString().PadLeft(2, '0');
        }

        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceVehiculoLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
        #endregion

        #region "Timer"
        protected override void OnStart(string[] args)
        {
            WriteToFile("Service is started at " + DateTime.Now);
            WriteToFile("Version 1.0.4");
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = Convert.ToDouble(ObtenerParametro("CLOUDFLEET.VEHICULO.TIMER"));
            WriteToFile("intervalo: " + timer.Interval);
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            OnDebug();
        }
        #endregion
    }
}
