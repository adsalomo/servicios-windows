USE [master]
GO
/****** Object:  Database [Aplicativos]    Script Date: 22/10/2019 12:29:48 ******/
CREATE DATABASE [Aplicativos]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Aplicativos', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Aplicativos.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Aplicativos_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Aplicativos_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Aplicativos] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Aplicativos].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Aplicativos] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Aplicativos] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Aplicativos] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Aplicativos] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Aplicativos] SET ARITHABORT OFF 
GO
ALTER DATABASE [Aplicativos] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Aplicativos] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Aplicativos] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Aplicativos] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Aplicativos] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Aplicativos] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Aplicativos] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Aplicativos] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Aplicativos] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Aplicativos] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Aplicativos] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Aplicativos] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Aplicativos] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Aplicativos] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Aplicativos] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Aplicativos] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Aplicativos] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Aplicativos] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Aplicativos] SET  MULTI_USER 
GO
ALTER DATABASE [Aplicativos] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Aplicativos] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Aplicativos] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Aplicativos] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Aplicativos] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Aplicativos] SET QUERY_STORE = OFF
GO
USE [Aplicativos]
GO
/****** Object:  Table [dbo].[auditoria_proceso]    Script Date: 22/10/2019 12:29:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auditoria_proceso](
	[template_id] [varchar](300) NOT NULL,
	[audit_id] [varchar](300) NOT NULL,
	[nombre_plantilla] [varchar](max) NULL,
	[created_at] [varchar](max) NULL,
	[modified_at] [varchar](max) NULL,
	[score] [varchar](max) NULL,
	[total_score] [varchar](max) NULL,
	[score_percentage] [varchar](max) NULL,
	[date_completed] [varchar](max) NULL,
	[date_modified] [varchar](max) NULL,
	[date_started] [varchar](max) NULL,
	[question_id] [varchar](300) NOT NULL,
	[question] [varchar](max) NULL,
	[response] [varchar](max) NULL,
	[note] [varchar](max) NULL,
	[station] [varchar](max) NULL,
	[position] [varchar](max) NULL,
	[developed_by] [varchar](max) NULL,
	[airline] [varchar](max) NULL,
 CONSTRAINT [PK_auditoria_proceso] PRIMARY KEY CLUSTERED 
(
	[template_id] ASC,
	[audit_id] ASC,
	[question_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ordenes]    Script Date: 22/10/2019 12:29:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ordenes](
	[number] [bigint] NOT NULL,
	[vehicleCode] [nvarchar](20) NULL,
	[workshopDate] [nvarchar](40) NULL,
	[startDate] [nvarchar](40) NULL,
	[estimatedFinishDate] [nvarchar](40) NULL,
	[status] [nvarchar](100) NULL,
	[odometer] [nvarchar](20) NULL,
	[vendedor_id] [nvarchar](20) NULL,
	[vendedor_name] [nvarchar](50) NULL,
	[reason] [varchar](500) NULL,
	[detectedIssue] [nvarchar](500) NULL,
	[paymentCondition] [nvarchar](200) NULL,
	[dwarranty] [nvarchar](40) NULL,
	[comments] [varchar](900) NULL,
	[type] [nvarchar](40) NULL,
	[city_id] [nvarchar](20) NULL,
	[city.name] [nvarchar](20) NULL,
	[city.code] [nvarchar](20) NULL,
	[costCenter_id] [nvarchar](20) NULL,
	[costCenter.name] [nvarchar](20) NULL,
	[costCenter.code] [nvarchar](20) NULL,
	[primaryGroup] [nvarchar](100) NULL,
	[secundaryGroup] [nvarchar](100) NULL,
	[createdAt] [nvarchar](40) NULL,
	[createdBy_id] [nvarchar](20) NULL,
	[createdBy_name] [nvarchar](50) NULL,
	[affectsMaintenanceSchedule] [nvarchar](20) NULL,
	[affectsVehicleAvailability] [nvarchar](20) NULL,
	[updatedAt] [nvarchar](20) NULL,
	[updatedBy_id] [nvarchar](20) NULL,
	[updatedBy_name] [nvarchar](50) NULL,
	[driver_id] [varchar](40) NULL,
	[driver_name] [varchar](100) NULL,
 CONSTRAINT [PK__ordenes__FD291E406434F6B1] PRIMARY KEY CLUSTERED 
(
	[number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[parametros]    Script Date: 22/10/2019 12:29:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[parametros](
	[Parametro] [nvarchar](200) NOT NULL,
	[Valor] [nvarchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Parametro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[plantillas]    Script Date: 22/10/2019 12:29:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[plantillas](
	[template_id] [varchar](100) NOT NULL,
	[name] [varchar](500) NOT NULL,
 CONSTRAINT [PK_plantillas] PRIMARY KEY CLUSTERED 
(
	[template_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[vehiculos]    Script Date: 22/10/2019 12:29:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vehiculos](
	[id] [bigint] NOT NULL,
	[code] [nvarchar](max) NULL,
	[typeName] [nvarchar](max) NULL,
	[brandName] [nvarchar](max) NULL,
	[lineName] [nvarchar](max) NULL,
	[city.id] [nvarchar](max) NULL,
	[city.name] [nvarchar](max) NULL,
	[city.code] [nvarchar](max) NULL,
	[costCenter.id] [nvarchar](max) NULL,
	[costCenter.name] [nvarchar](max) NULL,
	[costCenter.code] [nvarchar](max) NULL,
	[primaryGroup] [nvarchar](max) NULL,
	[secundaryGroup] [nvarchar](max) NULL,
	[hoursShouldWork] [nvarchar](max) NULL,
	[hoursInMaintenance] [nvarchar](max) NULL,
	[availability] [nvarchar](max) NULL,
	[start_date] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [Aplicativos] SET  READ_WRITE 
GO

insert into plantillas values ('template_7139af05e1ad493d97e406c09bbcc90e', 'SELF AUDIT VIBRAS- CONVENCIÓN 2019');

insert into plantillas values ('template_23f636b34afb4b3aa243fff1dd48fe82', 'SELF AUDIT - DESEMPEÑO DEL CONDUCTOR');
   
insert into plantillas values ('template_4fdae84dba7c40e8a98ec0a1f46e47dd', 'SELF AUDIT - PROCESO DE RECAUDOS');

insert into plantillas values ('template_472e9bf9aca64a8b818b19b5aa9e34ef', 'SELF AUDIT - DESEMPEÑO DE LIDERES AT - TTO AERONAVE ');

insert into plantillas values ('template_efe7f62c26374fcaa99e5018c459105d', 'SELF AUDIT - LLEGADA DE LA AERONAVE');

insert into plantillas values ('template_dd6ab88ed0d346c6a9a832c7a90a28b8', 'SELF AUDIT - SERVICIO AL PASAJERO ATENCIÓN EN MÓDULO');

insert into plantillas values ('template_6aeaf098fd094d2aaedef435e8814f6b', 'SELF AUDIT - SERVICIO AL PASAJERO SERVICIO EN SALA');

insert into plantillas values ('template_895f8816d23a4f03b799dc6e90bc8efb', 'SELF AUDIT - ACOPLE / ADOSAMIENTO DE EQUIPOS A LA AERONAVE');

insert into plantillas values ('template_2f3d41bd27e24df0861cd6c899e93432', 'SELF AUDIT - CARGUE DE LA AERONAVE');

INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'CLOUDFLEET.ORDENES.FECHA_DESDE', N'getdate-1')
INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'CLOUDFLEET.ORDENES.FECHA_HASTA', N'getdate')
INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'CLOUDFLEET.ORDENES.TIMER', N'8640000')
INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'CLOUDFLEET.TOKEN', N'sl4R$e@._+g36k0fPSZ5kcA*YT8Cc=DaP1@mIK5eD')
INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'CLOUDFLEET.VEHICULO.FECHA_DESDE', N'getdate-1')
INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'CLOUDFLEET.VEHICULO.FECHA_HASTA', N'getdate')
INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'CLOUDFLEET.VEHICULO.TIMER', N'8640000')
INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'SAFETYCULTURE.INSPECCION.FECHA_DESDE', N'getdate-140')
INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'SAFETYCULTURE.INSPECCION.TIMER', N'600000')
INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'SAFETYCULTURE.PASSWORD', N'Adm2018**')
INSERT [dbo].[parametros] ([Parametro], [Valor]) VALUES (N'SAFETYCULTURE.USUARIO', N'lideresgicentro%40gmail.com')
