using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Service_Obtener_Inspecciones
{
    [Table("plantillas")]
    public partial class Plantillas
    {
        [Key]
        [Column("template_id")]
        [StringLength(100)]
        public string TemplateId { get; set; }

        [Required]
        [Column("name")]
        [StringLength(500)]
        public string Name { get; set; }
    }
}
