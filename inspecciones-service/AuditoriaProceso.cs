﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Service_Obtener_Inspecciones
{
    [Table("auditoria_proceso")]
    public class AuditoriaProceso
    {
        [Key, Column("template_id", Order = 0)]
        [StringLength(300)]
        public string TemplateId { get; set; }

        [Key, Column("audit_id", Order = 1)]
        [StringLength(300)]
        public string AuditoriaId { get; set; }

        [Key, Column("question_id", Order = 2)]
        [StringLength(300)]
        public string PreguntaId { get; set; }

        [Column("created_at")]
        public String CreatedAt { get; set; }

        [Column("modified_at")]
        public String ModifiedAt { get; set; }

        [Column("score")]
        public String Score { get; set; }

        [Column("nombre_plantilla")]
        public String NombrePlantilla { get; set; }

        [Column("total_score")]
        public String TotalScore { get; set; }

        [Column("score_percentage")]
        public String ScorePercentage { get; set; }

        [Column("date_completed")]
        public String DateCompleted { get; set; }

        [Column("date_modified")]
        public String DateModified { get; set; }

        [Column("date_started")]
        public String DateStarted { get; set; }
        
        [Column("question")]
        public String Pregunta { get; set; }

        [Column("response")]
        public String Respuesta { get; set; }

        [Column("note")]
        public String Note { get; set; }

        [Column("station")]
        public String Station { get; set; }

        [Column("position")]
        public String Position { get; set; }

        [Column("developed_by")]
        public String DevelopedBy { get; set; }

        [Column("airline")]
        public String Airline { get; set; }
    }
}
