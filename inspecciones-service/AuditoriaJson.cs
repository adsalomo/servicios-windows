﻿using System;

namespace Service_Obtener_Inspecciones
{
    public class Authorship
    {
        public dynamic device_id { get; set; }
        public dynamic owner { get; set; }
        public dynamic owner_id { get; set; }
        public dynamic author { get; set; }
        public dynamic author_id { get; set; }
    }

    public class Site
    {
        public dynamic name { get; set; }
    }

    public class AuditData
    {
        public dynamic score { get; set; }
        public dynamic total_score { get; set; }
        public dynamic score_percentage { get; set; }
        public dynamic name { get; set; }
        public dynamic duration { get; set; }
        public Authorship authorship { get; set; }
        public dynamic date_completed { get; set; }
        public dynamic date_modified { get; set; }
        public dynamic date_started { get; set; }
        public Site site { get; set; }
    }

    public class TemplateData
    {
        public dynamic authorship { get; set; }
        public dynamic metadata { get; set; }
        public dynamic response_sets { get; set; } // lista, datos a persistir
    }

    public class Scoring
    {
        public dynamic combined_score { get; set; }
        public dynamic combined_max_score { get; set; }
        public dynamic combined_score_percentage { get; set; }
    }

    public class Item
    {
        public dynamic parent_id { get; set; }
        public dynamic item_id { get; set; }  // identificador, puede ser parent_id al ser padre de otro item
        public dynamic label { get; set; }
        public dynamic type { get; set; }
        public Scoring scoring { get; set; } // revisar contenido
        public dynamic children { get; set; } // lista de hijos que pueden ser hijos
        public dynamic options { get; set; }
    }
    
    public class AuditoriaJson
    {
        public String template_id { get; set; } //String
        public String audit_id { get; set; } // String
        public dynamic archived { get; set; } // boolean
        public dynamic created_at { get; set; } // Date
        public dynamic modified_at { get; set; } // Date
        public AuditData audit_data { get; set; }
        public TemplateData template_data { get; set; }
        public dynamic items { get; set; }
        public dynamic header_items { get; set; }
    }

}
