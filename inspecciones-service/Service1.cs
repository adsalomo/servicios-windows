﻿using System;
using System.Data;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using RestSharp;
using Newtonsoft.Json;
using System.Data.Entity;

namespace Service_Obtener_Inspecciones
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer(); // name space(using System.Timers;)  
        private AplicativoDB context = new AplicativoDB();
        private static readonly string VAR_ESTACION_1 = "Estación";
        private static readonly string VAR_ESTACION_2 = "Estacion";
        private static readonly string VAR_AEROLINEA_1 = "Aerolínea";
        private static readonly string VAR_AEROLINEA_2 = "Aerolinea";
        private static readonly string VAR_DESARROLLADO_POR_1 = "Desarrollada por";
        private static readonly string VAR_DESARROLLADO_POR_2 = "Realizada Por";
        private static readonly string VAR_CARGO_1 = "Cargo de Quien Realiza la Inspección";
        private static readonly string VAR_CARGO_2 = "Cargo de Quien Realiza la Inspeccion";

        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            EjecutarProceso();
        }

        private void EjecutarProceso()
        {
            try
            {
                // Plantillas de la base de datos
                Utils.Utils.WriteToFile("Se consultan las plantillas de la base de datos.");
                var plantillas = from p in context.Plantillas select p;
                if (plantillas == null || plantillas.ToList() == null || plantillas.ToList().Count == 0)
                {
                    return;
                }

                // Parametros
                Utils.Utils.WriteToFile("Se consultan los parametros de la base de datos.");
                var parametros = from p in context.Parametro select p;
                if (parametros == null || parametros.ToList() == null || parametros.ToList().Count == 0)
                {
                    return;
                }

                string fechaDesde = parametros.ToList()
                    .Find(x => x.ParametroId.Equals(Utils.Utils.PARAMETRO_FECHA_DESDE)).Valor;
                fechaDesde = Utils.Utils.ConvertirFecha(fechaDesde != null ? fechaDesde : "");

                string usuario = parametros.ToList()
                    .Find(x => x.ParametroId.Equals(Utils.Utils.PARAMETRO_USUARIO)).Valor;
                if (usuario == null || usuario.Equals(""))
                {
                    Utils.Utils.WriteToFile("El proceso termina, usuario vacio o nulo.");
                    return;
                }

                string password = parametros.ToList()
                    .Find(x => x.ParametroId.Equals(Utils.Utils.PARAMETRO_PASSWORD)).Valor;
                if (password == null || password.Equals(""))
                {
                    Utils.Utils.WriteToFile("El proceso termina, password vacio o nulo.");
                    return;
                }

                // Seguridad Token
                Utils.Utils.WriteToFile("Se realiza el proceso de autenticacion.");
                Utils.Utils.WriteToFile("URL: https://api.safetyculture.io/auth");
                IRestResponse response = Utils.Utils.Autenticar("https://api.safetyculture.io/auth",
                    usuario, password);
                if (!Utils.Utils.ValidarRespuesta(response))
                {
                    return;
                }

                var objsecurity = JsonConvert.DeserializeObject<Token>(response.Content);

                StringBuilder builderUrl = new StringBuilder();

                Utils.Utils.WriteToFile("Inicio lectura plantillas at " + DateTime.Now);

                foreach (var plantilla in plantillas.ToList())
                {
                    // Obtenemos templates
                    Utils.Utils.WriteToFile("Se consulta el servicio de templates.");
                    builderUrl.Clear();
                    builderUrl.Append("https://api.safetyculture.io/audits/search?");
                    builderUrl.Append("template=").Append(plantilla.TemplateId);
                    builderUrl.Append("&modified_after=").Append(fechaDesde);
                    Utils.Utils.WriteToFile("URL: " + builderUrl.ToString());
                    response = Utils.Utils.EjecutarGet(builderUrl.ToString(), objsecurity.access_token);

                    if (!Utils.Utils.ValidarRespuesta(response))
                    {
                        continue;
                    }

                    var objinspecciones = JsonConvert.DeserializeObject<InspeccionesJson>(response.Content);
                    if (objinspecciones == null || objinspecciones.count == 0
                        || objinspecciones.audits == null || objinspecciones.audits.Count == 0)
                    {
                        Utils.Utils.WriteToFile("Auditorias para plantilla: "
                            + plantilla.TemplateId + " y fecha: " + fechaDesde + " no encontro registros.");
                        continue;
                    }

                    foreach (var insp in objinspecciones.audits)
                    {
                        // Consulta auditoria
                        Utils.Utils.WriteToFile("Se consulta el servicio de auditorias.");
                        builderUrl.Clear();
                        builderUrl.Append("https://api.safetyculture.io/audits/");
                        builderUrl.Append(insp.audit_id);
                        Utils.Utils.WriteToFile("URL: " + builderUrl.ToString());
                        response = Utils.Utils.EjecutarGet(builderUrl.ToString(), objsecurity.access_token);

                        if (!Utils.Utils.ValidarRespuesta(response))
                        {
                            continue;
                        }

                        var objAuditoria = JsonConvert.DeserializeObject<AuditoriaJson>(response.Content);
                        if (objAuditoria == null)
                        {
                            Utils.Utils.WriteToFile("Objeto auditoria esta vacio al consultar por la auditoria: "
                                + insp.audit_id);
                            continue;
                        }
                      
                        if (objAuditoria.header_items == null || objAuditoria.header_items.Count == 0)
                        {
                            Utils.Utils.WriteToFile("Objeto header_items vacio o nulo para auditoria: "
                                + insp.audit_id);
                            continue;
                        }

                        if (objAuditoria.items == null || objAuditoria.items.Count == 0)
                        {
                            Utils.Utils.WriteToFile("Objeto items vacio o nulo para auditoria: "
                                + insp.audit_id);
                            continue;
                        }

                        //Guardar(objAuditoria, objAuditoria.header_items);
                        Guardar(objAuditoria, objAuditoria.items); //items
                    }
                }
                Utils.Utils.WriteToFile("Fin proceso lectura de plantillas at " + DateTime.Now);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.InnerException != null && ex.InnerException.InnerException.Message != null)
                {
                    Utils.Utils.WriteToFile("Error InnerException " + ex.InnerException.InnerException.Message);
                    Console.WriteLine(ex.InnerException.Message);
                }

                Utils.Utils.WriteToFile("Error GENERAL " + ex.StackTrace.ToString());
            }
        }

        private string GetValorHeader(dynamic items, string valorVerificar)
        {
            string valor = "";
            foreach (var row in items)
            {
                if (row == null)
                {
                    continue;
                }

                string tipo = Convert.ToString(row.type);
                if (!tipo.Equals("question") && !tipo.Equals("list") && !tipo.Equals("text")
                    && !tipo.Equals("textsingle") && !tipo.Equals("datetime") && !tipo.Equals("address"))
                {
                    continue;
                }

                if (row.label != null && row.label.ToString().ToUpper().Contains(valorVerificar.ToString().ToUpper()))
                {
                    if (row.responses != null)
                    {
                        if (row.responses.text != null && !string.IsNullOrEmpty(Convert.ToString(row.responses.text)))
                        {
                            valor = row.responses.text;
                            break;
                        }

                        if (row.responses.selected != null && row.responses.selected.Count > 0)
                        {
                            foreach (var item in row.responses.selected)
                            {
                                valor = item.label;
                                break;
                            }
                            break;
                        }
                        else if (row.responses.location_text != null)
                        {
                            valor = row.responses.location_text;
                            break;
                        }
                        else if (row.responses.datetime != null)
                        {
                            valor = row.responses.datetime;
                            break;
                        }
                        else
                        {
                            valor = null;
                            break;
                        }
                    }
                }
            }

            return valor;
        }

        private void Guardar(AuditoriaJson objAuditoria, dynamic items)
        {
            string preguntaId = "";
            try
            {
                foreach (var row in items)
                {
                    if (row == null)
                    {
                        continue;
                    }

                    string tipo = Convert.ToString(row.type);
                    if (!tipo.Equals("question") && !tipo.Equals("list") && !tipo.Equals("text") 
                        && !tipo.Equals("textsingle") && !tipo.Equals("datetime") && !tipo.Equals("address"))
                    {
                        continue;
                    }

                    preguntaId = Convert.ToString(row.item_id);
                    var auditoriaProceso = context.AuditoriaProceso
                        .Where(x => x.TemplateId != null && x.TemplateId.Equals(objAuditoria.template_id)
                                 && x.AuditoriaId != null && x.AuditoriaId.Equals(objAuditoria.audit_id)
                                 && x.PreguntaId != null && x.PreguntaId.Equals(preguntaId))
                        .SingleOrDefault();

                    if (auditoriaProceso != null)
                    {
                        SetDatos(auditoriaProceso, objAuditoria, row); 
                        context.AuditoriaProceso.Attach(auditoriaProceso);
                        context.Entry(auditoriaProceso).State = EntityState.Modified;
                    }
                    else
                    {
                        auditoriaProceso = new AuditoriaProceso();
                        auditoriaProceso.TemplateId = objAuditoria.template_id;
                        auditoriaProceso.AuditoriaId = objAuditoria.audit_id;
                        auditoriaProceso.PreguntaId = preguntaId;
                        // Nombre plantilla
                        if (objAuditoria.template_data != null && objAuditoria.template_data.metadata != null && objAuditoria.template_data.metadata.name != null)
                        {
                            auditoriaProceso.NombrePlantilla = objAuditoria.template_data.metadata.name;
                        }
                        else
                        {
                            auditoriaProceso.NombrePlantilla = null;
                        }
                        SetDatos(auditoriaProceso, objAuditoria, row);
                        context.AuditoriaProceso.Attach(auditoriaProceso);
                        context.Entry(auditoriaProceso).State = EntityState.Added;
                    }

                    // Guardar cambios
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Utils.Utils.WriteToFile("Error al guardar template_id: " + objAuditoria.template_id);
                Utils.Utils.WriteToFile("Error al guardar auditoria_id: " + objAuditoria.audit_id);
                Utils.Utils.WriteToFile("Error al guardar pregunta_id: " + preguntaId);
                if (ex.InnerException != null && ex.InnerException.InnerException != null && ex.InnerException.InnerException.Message != null)
                {
                    Utils.Utils.WriteToFile("Error InnerException " + ex.InnerException.InnerException.Message);
                    Console.WriteLine(ex.InnerException.Message);
                }
                Utils.Utils.WriteToFile("Error: " + ex.StackTrace.ToString());
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }

        private void SetDatos(AuditoriaProceso auditoriaProceso, AuditoriaJson objAuditoria, dynamic row)
        {
            SetValoresEstaticos(auditoriaProceso, objAuditoria);

            auditoriaProceso.CreatedAt = objAuditoria.created_at != null ? Convert.ToString(objAuditoria.created_at) : "";
            auditoriaProceso.ModifiedAt = objAuditoria.modified_at != null ? Convert.ToString(objAuditoria.modified_at) : "";

            if (objAuditoria.audit_data != null)
            {
                auditoriaProceso.Score = objAuditoria.audit_data.score != null ? Convert.ToString(objAuditoria.audit_data.score) : "0";
                auditoriaProceso.TotalScore = objAuditoria.audit_data.total_score != null ? Convert.ToString(objAuditoria.audit_data.total_score) : "0";
                auditoriaProceso.ScorePercentage = objAuditoria.audit_data.score_percentage != null ? Convert.ToString(objAuditoria.audit_data.score_percentage) : "0";
                auditoriaProceso.DateCompleted = objAuditoria.audit_data.date_completed != null ? Convert.ToString(objAuditoria.audit_data.date_completed) : "";
                auditoriaProceso.DateModified = objAuditoria.audit_data.date_modified != null ? Convert.ToString(objAuditoria.audit_data.date_modified) : "";
                auditoriaProceso.DateStarted = objAuditoria.audit_data.date_started != null ? Convert.ToString(objAuditoria.audit_data.date_started) : "";
            }

            auditoriaProceso.Pregunta = row.label;

            if (row.responses != null)
            {
                if (row.responses.text != null && !string.IsNullOrEmpty(Convert.ToString(row.responses.text)))
                {
                    auditoriaProceso.Note = row.responses.text;
                }

                if (row.responses.selected != null && row.responses.selected.Count > 0)
                {
                    foreach (var item in row.responses.selected)
                    {
                        auditoriaProceso.Respuesta = item.label;
                        break;
                    }
                } else if (row.responses.location_text != null)
                {
                    auditoriaProceso.Respuesta = row.responses.location_text;
                } else if (row.responses.datetime != null)
                {
                    auditoriaProceso.Respuesta = row.responses.datetime;
                }
                else
                {
                    auditoriaProceso.Respuesta = null;
                }
            }
        }

        private void SetValoresEstaticos(AuditoriaProceso auditoriaProceso, AuditoriaJson objAuditoria)
        {
            string valor = GetValorHeader(objAuditoria.header_items, VAR_ESTACION_1);
            if (string.IsNullOrEmpty(valor))
            {
                valor = GetValorHeader(objAuditoria.header_items, VAR_ESTACION_2);
            }
            auditoriaProceso.Station = valor;

            valor = GetValorHeader(objAuditoria.header_items, VAR_CARGO_1);
            if (string.IsNullOrEmpty(valor))
            {
                valor = GetValorHeader(objAuditoria.header_items, VAR_CARGO_2);
            }
            auditoriaProceso.Position = valor;

            valor = GetValorHeader(objAuditoria.header_items, VAR_DESARROLLADO_POR_1);
            if (string.IsNullOrEmpty(valor))
            {
                valor = GetValorHeader(objAuditoria.header_items, VAR_DESARROLLADO_POR_2);
            }
            auditoriaProceso.DevelopedBy = valor;

            valor = GetValorHeader(objAuditoria.header_items, VAR_AEROLINEA_1);
            if (string.IsNullOrEmpty(valor))
            {
                valor = GetValorHeader(objAuditoria.header_items, VAR_AEROLINEA_2);
            }
            auditoriaProceso.Airline = valor;
        }

        protected override void OnStart(string[] args)
        {
            Utils.Utils.WriteToFile("Service is started at " + DateTime.Now);
            Utils.Utils.WriteToFile("version 1.0.3");
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);

            var parametros = from p in context.Parametro select p;
            if (parametros == null || parametros.ToList() == null || parametros.ToList().Count == 0)
            {
                return;
            }

            string interval = parametros.ToList()
                    .Find(x => x.ParametroId.Equals(Utils.Utils.PARAMETRO_INTERVALO_TIMER)).Valor;
            if (string.IsNullOrEmpty(interval))
            {
                return;
            }

            timer.Interval = Convert.ToDouble(interval);
            Utils.Utils.WriteToFile("Intervalo: " + timer.Interval);
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            Utils.Utils.WriteToFile("Service is stopped at " + DateTime.Now);
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            EjecutarProceso();
        }
    }
}
