﻿using System.Collections.Generic;

namespace Service_Obtener_Inspecciones
{

    public class Audit
    {
        public string audit_id { get; set; }
        public string modified_at { get; set; }
        public string template_id { get; set; }
    }

    public class InspeccionesJson
    {
        public int count { get; set; }
        public int total { get; set; }
        public List<Audit> audits { get; set; }
    }
}
