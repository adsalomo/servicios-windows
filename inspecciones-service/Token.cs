﻿namespace Service_Obtener_Inspecciones
{
    public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
}
