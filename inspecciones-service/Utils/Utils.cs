﻿using Newtonsoft.Json;
using RestSharp;
using System;

using System.IO;
using System.Net;

namespace Service_Obtener_Inspecciones.Utils
{
    public class Utils
    {
        public readonly static string PARAMETRO_FECHA_DESDE = "SAFETYCULTURE.INSPECCION.FECHA_DESDE";
        public readonly static string PARAMETRO_USUARIO = "SAFETYCULTURE.USUARIO";
        public readonly static string PARAMETRO_PASSWORD = "SAFETYCULTURE.PASSWORD";
        public readonly static string PARAMETRO_INTERVALO_TIMER = "SAFETYCULTURE.INSPECCION.TIMER";

        public static string ConvertirFecha(string pvalor)
        {
            DateTime fecha;
            if (pvalor.ToUpper() == "GETDATE")
            {
                fecha = DateTime.Today;
            }
            else if (pvalor.Contains("-1"))
            {
                string dias = pvalor.ToUpper().Replace("GETDATE", "");
                fecha = DateTime.Today.AddDays(Convert.ToInt16(dias));
            } else
            {
                fecha = DateTime.Today;
            }
            return fecha.Year.ToString() + '-' + fecha.Month.ToString().PadLeft(2, '0') + '-' + fecha.Day.ToString().PadLeft(2, '0');
        }

        public static IRestResponse Autenticar(string url, string usuario, string contrasena)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.DefaultConnectionLimit = 9999;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("Host", "api.safetyculture.io");
            request.AddHeader("Cache-Control", "no-cache");
            string URLSECURYTY = "username=" + usuario + "&password=" + contrasena + "&grant_type=password";
            request.AddParameter("application/x-www-form-urlencoded", URLSECURYTY, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static IRestResponse EjecutarGet(string url, string security)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.DefaultConnectionLimit = 9999;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("Host", "api.safetyculture.io");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Host", "api.safetyculture.io");

            request.AddHeader("Authorization", "Bearer " + security);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static bool ValidarRespuesta(IRestResponse response)
        {
            WriteToFile("Se procede a validar el objeto obtenido del servicio.");
            if (response != null)
            {
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    dynamic dynJsonerror = JsonConvert.DeserializeObject(response.Content);
                    if (dynJsonerror != null)
                    {
                        foreach (var item in dynJsonerror)
                        {
                            if (item != null)
                            {
                                string mensaje = ((Newtonsoft.Json.Linq.JContainer)item.First).First.ToString();
                                WriteToFile("La Api devolvio un mensaje de error: " + mensaje);
                            }
                        }
                    }

                    if (response.ErrorException != null && !response.ErrorException.Equals(""))
                    {
                        WriteToFile("Error al consumir servicio: " + response.ErrorException);
                    }

                    if (response.ErrorMessage != null && !response.ErrorMessage.Equals(""))
                    {
                        WriteToFile("Error al consumir servicio: " + response.ErrorMessage);
                    }

                    return false;
                }
            }

            WriteToFile("Objeto validado sin errores.");
            return true;
        }

        public static void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (!Directory.Exists(path))
            {
                return;
            }

            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceInspeccionLog_" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
