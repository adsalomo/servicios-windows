namespace Service_Obtener_Inspecciones
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AplicativoDB : DbContext
    {
        public AplicativoDB()
            : base("name=AplicativoDB")
        {
        }

        public virtual DbSet<Parametro> Parametro { get; set; }
        public virtual DbSet<Plantillas> Plantillas { get; set; }
        public virtual DbSet<AuditoriaProceso> AuditoriaProceso { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}
