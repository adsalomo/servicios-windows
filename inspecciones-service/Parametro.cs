namespace Service_Obtener_Inspecciones
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("parametros")]
    public partial class Parametro
    {
        [Key]
        [Column("parametro")]
        [StringLength(200)]
        public string ParametroId { get; set; }

        [Required]
        [Column("valor")]
        [StringLength(200)]
        public string Valor { get; set; }
    }
}
