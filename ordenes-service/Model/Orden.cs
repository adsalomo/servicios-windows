﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_Obtener_Ordenes.Model
{
    public class Orden
    {
        public class Vendor
        {
            public int id { get; set; }
            public string name { get; set; }
        }

        public class City
        {
            public int id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
        }

        public class CostCenter
        {
            public int id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
        }

        public class CreatedBy
        {
            public int id { get; set; }
            public string name { get; set; }
        }

        public class UpdatedBy
        {
            public int id { get; set; }
            public string name { get; set; }
        }

        public class RootObject
        {
            public int number { get; set; }
            public string vehicleCode { get; set; }
            public DateTime workshopDate { get; set; }
            public DateTime startDate { get; set; }
            public DateTime estimatedFinishDate { get; set; }
            public string status { get; set; }
            public int odometer { get; set; }
            public Vendor vendor { get; set; }
            public string reason { get; set; }
            public string detectedIssue { get; set; }
            public object paymentCondition { get; set; }
            public object warranty { get; set; }
            public string comments { get; set; }
            public object driver { get; set; }
            public object maintenanceLabels { get; set; }
            public object type { get; set; }
            public City city { get; set; }
            public CostCenter costCenter { get; set; }
            public object primaryGroup { get; set; }
            public object secundaryGroup { get; set; }
            public object createdAt { get; set; }
            public CreatedBy createdBy { get; set; }
            public bool affectsMaintenanceSchedule { get; set; }
            public bool affectsVehicleAvailability { get; set; }
            public DateTime updatedAt { get; set; }
            public UpdatedBy updatedBy { get; set; }
        }
    }
}
