﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using RestSharp;
using Newtonsoft.Json;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace Service_Obtener_Ordenes
{
    public partial class Service1 : ServiceBase
    {
        private AplicativoDB db = new AplicativoDB();
        private Timer timer = new Timer();
        private readonly string KEY_NEXT_PAGE = "X-NextPage";

        private string fecha_desde = "";
        private string fecha_hasta = "";

        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            WriteToFile("Service is recall at " + DateTime.Now);
            fecha_desde = ConvertirFecha(ObtenerParametro("CLOUDFLEET.ORDENES.FECHA_DESDE"));
            fecha_hasta = ConvertirFecha(ObtenerParametro("CLOUDFLEET.ORDENES.FECHA_HASTA"));
            WriteToFile("fecha_desde: " + fecha_desde);
            WriteToFile("fecha_hasta: " + fecha_hasta);
            string token = "Bearer " + ObtenerParametro("CLOUDFLEET.TOKEN");
            string url = "https://fleet.cloudfleet.com/api/v1/work-orders/?startDateFrom=" + fecha_desde + "&startDateTo=" + fecha_hasta;
            EjecutarProcesoOrdenes(url, token);
            WriteToFile("Finalizo la busqueda y actualizacion " + DateTime.Now);
        }

        public void EjecutarProcesoOrdenes(string url, string token)
        {
            try
            {
                WriteToFile("Ejecutando url: " + url);

                IRestResponse response = EjecutarGet(url, token);
                if (ValidarRespuesta(response))
                {
                    dynamic dynJson = JsonConvert.DeserializeObject(response.Content);
                    bool esnuevo = false;

                    foreach (var item in dynJson)
                    {
                        WriteToFile("Objeto a validar: " + item);
                        esnuevo = false;
                        long codigo = item.number;
                        Ordene ordenjson = db.ordenes.Where(x => x.number == codigo).SingleOrDefault();

                        if (ordenjson == null)
                        {
                            esnuevo = true;
                            ordenjson = new Ordene();
                        }

                        ordenjson.number = item.number;
                        ordenjson.vehicleCode = item.vehicleCode;
                        ordenjson.workshopDate = item.workshopDate;
                        ordenjson.startDate = item.startDate;
                        ordenjson.estimatedFinishDate = item.estimatedFinishDate;
                        ordenjson.status = item.status;
                        ordenjson.odometer = item.odometer;

                        if (item.vendedor != null)
                        {
                            ordenjson.vendedor_id = item.vendedor.id;
                            ordenjson.vendedor_name = item.vendedor.name;
                        }

                        if (item.detectedIssue != null)
                        {
                            ordenjson.detectedIssue = item.detectedIssue;
                        }

                        ordenjson.paymentCondition = item.paymentCondition;
                        ordenjson.dwarranty = item.dwarranty;
                        ordenjson.comments = item.comments;

                        if (item.driver != null)
                        {
                            ordenjson.driver_id = item.driver.id;
                            ordenjson.driver_name = item.driver.name;
                        }

                        //ordenjson.maintenanceLabels = item.maintenanceLabels;
                        ordenjson.type = item.type;

                        if (item.city != null)
                        {
                            ordenjson.city_id = item.city.id;
                            ordenjson.city_name = item.city.name;
                            ordenjson.city_code = item.city.code;
                        }

                        if (item.costCenter != null)
                        {
                            ordenjson.costCenter_id = item.costCenter.id;
                            ordenjson.costCenter_name = item.costCenter.name;
                            ordenjson.costCenter_code = item.costCenter.code;
                        }

                        ordenjson.primaryGroup = item.primaryGroup;
                        ordenjson.secundaryGroup = item.secundaryGroup;
                        ordenjson.createdAt = item.createdAt;

                        if (item.createdBy != null)
                        {
                            ordenjson.createdBy_id = item.createdBy.id;
                            ordenjson.createdBy_name = item.createdBy.name;
                        }

                        ordenjson.affectsMaintenanceSchedule = item.affectsMaintenanceSchedule;
                        ordenjson.affectsVehicleAvailability = item.affectsVehicleAvailability;
                        ordenjson.updatedAt = item.updatedAt;

                        if (item.updatedBy != null)
                        {
                            ordenjson.updatedBy_id = item.updatedBy.id;
                            ordenjson.updatedBy_name = item.updatedBy.name;
                        }

                        db.ordenes.Attach(ordenjson);

                        if (!esnuevo)
                        {
                            db.Entry(ordenjson).State = EntityState.Modified;
                        }
                        else
                        {
                            db.Entry(ordenjson).State = EntityState.Added;
                        }

                        db.SaveChanges();
                    }

                    if (response.Headers != null && response.Headers.ToList().Count > 0)
                    {
                        var headers = response.Headers.ToList().Find(x => x.Name.Equals(KEY_NEXT_PAGE));
                        if (headers != null && headers.Value != null)
                        {
                            EjecutarProcesoOrdenes(Convert.ToString(headers.Value), token);
                        }
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    WriteToFile("Error de Persistencia:" + string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: ", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        WriteToFile(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.InnerException != null && ex.InnerException.InnerException.Message != null)
                {
                    WriteToFile("Error InnerException " + ex.InnerException.InnerException.Message);
                    Console.WriteLine(ex.InnerException.Message);
                }
                WriteToFile("Error " + ex.StackTrace.ToString());
            }
        }

        #region "Utilidades"
        private static IRestResponse EjecutarGet(string url, string security)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");

            request.AddHeader("accept-encoding", "gzip, deflate");
            request.AddHeader("Host", "fleet.cloudfleet.com");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
            request.AddHeader("Authorization", security);
            request.AddHeader("Content-Type", "application/json; charset=utf-8");
            IRestResponse response = client.Execute(request);
            return response;
        }

        private bool ValidarRespuesta(IRestResponse response)
        {
            WriteToFile("Se procede a validar el objeto obtenido del servicio.");
            if (response != null)
            {
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    dynamic dynJsonerror = JsonConvert.DeserializeObject(response.Content);
                    if (dynJsonerror != null)
                    {
                        foreach (var item in dynJsonerror)
                        {
                            if (item != null)
                            {
                                string mensaje = ((Newtonsoft.Json.Linq.JContainer)item.First).First.ToString();
                                WriteToFile("La Api devolvio un mensaje de error: " + mensaje);
                            }
                        }
                    }

                    if (response.ErrorException != null && !response.ErrorException.Equals(""))
                    {
                        WriteToFile("Error al consumir servicio: " + response.ErrorException);
                    }

                    if (response.ErrorMessage != null && !response.ErrorMessage.Equals(""))
                    {
                        WriteToFile("Error al consumir servicio: " + response.ErrorMessage);
                    }

                    return false;
                }
            }

            WriteToFile("Objeto validado sin errores.");
            return true;
        }

        private string ObtenerParametro(string pparametro)
        {
            AplicativoDB dbparametro = new AplicativoDB();
            string valor = dbparametro.parametros.Where(x => x.Parametro1 == pparametro).First().Valor;
            dbparametro.Dispose();
            return valor;
        }

        private string ConvertirFecha(string pvalor)
        {
            DateTime fecha;
            if (pvalor.ToUpper() == "GETDATE")
            {
                fecha = DateTime.Today;
            }
            else
            {
                string dias = pvalor.ToUpper().Replace("GETDATE", "");
                fecha = DateTime.Today.AddDays(Convert.ToInt16(dias));
            }
            return fecha.Year.ToString() + '-' + fecha.Month.ToString().PadLeft(2, '0') + '-' + fecha.Day.ToString().PadLeft(2, '0');
        }

        private void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

#endregion

        #region "Timer"
        protected override void OnStart(string[] args)
        {

            WriteToFile("Service is started at " + DateTime.Now);
            WriteToFile("Version 1.0.21");
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = Convert.ToDouble(ObtenerParametro("CLOUDFLEET.ORDENES.TIMER"));
            WriteToFile("intervalo: " + timer.Interval);
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            OnDebug();
        }
#endregion
    }
}
