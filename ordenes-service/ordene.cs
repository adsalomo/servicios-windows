namespace Service_Obtener_Ordenes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Ordene
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long number { get; set; }

        //[StringLength(20)]
        public string vehicleCode { get; set; }

        //[StringLength(40)]
        public string workshopDate { get; set; }

        //[StringLength(40)]
        public string startDate { get; set; }

        //[StringLength(40)]
        public string estimatedFinishDate { get; set; }

        //[StringLength(100)]
        public string status { get; set; }

        //[StringLength(20)]
        public string odometer { get; set; }

        //[StringLength(20)]
        public string vendedor_id { get; set; }

        //[StringLength(50)]
        public string vendedor_name { get; set; }

        //[StringLength(400)]
        public string reason { get; set; }

        //[StringLength(400)]
        public string detectedIssue { get; set; }

        //[StringLength(200)]
        public string paymentCondition { get; set; }

        //[StringLength(40)]
        public string dwarranty { get; set; }

        //[StringLength(800)]
        public string comments { get; set; }

        //[StringLength(40)]
        //public string maintenanceLabels { get; set; }

        //[StringLength(40)]
        public string type { get; set; }

        //[StringLength(20)]
        public string city_id { get; set; }

        [Column("city.name")]
        //[StringLength(20)]
        public string city_name { get; set; }

        [Column("city.code")]
        //[StringLength(20)]
        public string city_code { get; set; }

        //[StringLength(20)]
        public string costCenter_id { get; set; }

        [Column("costCenter.name")]
        //[StringLength(20)]
        public string costCenter_name { get; set; }

        [Column("costCenter.code")]
        //[StringLength(20)]
        public string costCenter_code { get; set; }

        //[StringLength(100)]
        public string primaryGroup { get; set; }

        //[StringLength(100)]
        public string secundaryGroup { get; set; }

        //[StringLength(40)]
        public string createdAt { get; set; }

        //[StringLength(20)]
        public string createdBy_id { get; set; }

        //[StringLength(50)]
        public string createdBy_name { get; set; }

        //[StringLength(20)]
        public string affectsMaintenanceSchedule { get; set; }

        //[StringLength(20)]
        public string affectsVehicleAvailability { get; set; }

        //[StringLength(20)]
        public string updatedAt { get; set; }

        //[StringLength(20)]
        public string updatedBy_id { get; set; }

        //[StringLength(50)]
        public string updatedBy_name { get; set; }

        //[StringLength(40)]
        public string driver_id { get; set; }

        //[StringLength(100)]
        public string driver_name { get; set; }
    }
}
