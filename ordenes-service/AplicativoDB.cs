namespace Service_Obtener_Ordenes
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AplicativoDB : DbContext
    {
        public AplicativoDB()
            : base("name=AplicativoDB")
        {
        }

        public virtual DbSet<Ordene> ordenes { get; set; }
        public virtual DbSet<Parametro> parametros { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
